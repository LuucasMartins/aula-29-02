package br.sc.senai.hello.controller;
import javax.inject.Named;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;

@Named(value="CadastroPlayerController")
@SessionScoped
public class CadastroPlayerController implements Serializable {

private static final long serialVersionUID = 1L;

private String nomeJogador;
private String sobrenomeJogador;
private String idadeJogador;

public String getNomeJogador() {
	return nomeJogador;
}

public void setNomeJogador(String nomeJogador) {
	this.nomeJogador = nomeJogador;
}

public String getSobrenomeJogador() {
	return sobrenomeJogador;
}

public void setSobrenomeJogador(String sobrenomeJogador) {
	this.sobrenomeJogador = sobrenomeJogador;
}

public String getIdadeJogador() {
	return idadeJogador;
}

public void setIdadeJogador(String idadeJogador) {
	this.idadeJogador = idadeJogador;
}



}