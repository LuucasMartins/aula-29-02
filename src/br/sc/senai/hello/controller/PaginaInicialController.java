package br.sc.senai.hello.controller;

import javax.inject.Named;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;

@Named(value="testeController")
@SessionScoped

public class PaginaInicialController implements Serializable {

private static final long serialVersionUID = 1L;

private String dizeres = "Katchau";

public String getDizeres() {
	return dizeres;
}

public void setDizeres(String dizeres) {
	this.dizeres = dizeres;
}



}